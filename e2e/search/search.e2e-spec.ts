import { SearchPageObject } from "./search.po";
/**
 * Created by pavlos on 25/12/2016.
 */
describe("search component", () => {
  let page = new SearchPageObject();

  beforeEach(()=>{
    page.navigateTo();
  });

  it("should search for project", () => {
    page.getSearchInputElement().sendKeys("saferandom");
    let results = page.getSearchResults();
    expect(results.count()).toBeGreaterThanOrEqual(1);
  });

});