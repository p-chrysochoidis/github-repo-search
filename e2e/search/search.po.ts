/**
 * Created by pavlos on 25/12/2016.
 */
import { browser, element, by } from "protractor";

export class SearchPageObject {
  navigateTo() {
    return browser.get('/search');
  }

  getSearchInputElement(){
    return element(by.css("app-search-trigger input.search-input"));
  }

  getSearchResults(){
    return element.all(by.css("app-search-result-item"));
  }
}