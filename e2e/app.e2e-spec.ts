import { GithubRepoSearchPage } from './app.po';
import { browser } from "protractor";

describe('github-repo-search App', function() {
  let page: GithubRepoSearchPage;

  beforeEach(() => {
    page = new GithubRepoSearchPage();
    page.navigateTo();
  });

  it('should redirect to search view', () => {
    expect(browser.getCurrentUrl()).toBe("http://localhost:4200/search");
  });

});
