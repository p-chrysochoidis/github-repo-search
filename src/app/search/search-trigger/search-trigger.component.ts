import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { Subscription } from "rxjs";
import { FormControl } from "@angular/forms";

/**
 * A component for managing the user's input.
 *
 * Inputs:
 * {@link promptText} A string to show to the user as a prompt
 * {@link searchTerm} Used from the parent component to initialize or change
 * the search term
 *
 * Outputs:
 * {@link searchQueryChanged} Emits the new query to search.
 */
@Component({
  selector: 'app-search-trigger',
  templateUrl: './search-trigger.component.html',
  styleUrls: ['./search-trigger.component.scss']
})
export class SearchTriggerComponent implements OnInit, OnDestroy, OnChanges {
  @Input()
  private promptText:string;
  @Input()
  private searchTerm:string;
  @Output()
  private searchQueryChanged: EventEmitter<string>;

  private changeSubscription: Subscription;
  private searchInput: FormControl;

  constructor() {
    this.searchQueryChanged = new EventEmitter<string>();
    this.searchInput = new FormControl("");
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes['searchTerm']){
      this.searchInput.setValue(this.searchTerm);
    }
  }

  ngOnInit() {
    this.subscribeForChanges();
  }

  ngOnDestroy(){
    this.unsubscribeFromChanges();
  }

  /**
   * Subscribes for search query changes with {@link searchInput} and triggers the
   * {@link searchQueryChanged} event after debouching the change.
   */
  private subscribeForChanges(){
    let changeStream = this.searchInput.valueChanges.debounceTime(200);
    this.changeSubscription = changeStream.subscribe((searchQuery) => {
      this.searchQueryChanged.next(searchQuery);
    });
  }

  /**
   * Call this method to unsubscribe from input changes observable.
   */
  private unsubscribeFromChanges(){
    if(this.changeSubscription){
      this.changeSubscription.unsubscribe();
    }
    this.changeSubscription = null;
  }
}