/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { SearchTriggerComponent } from './search-trigger.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

describe('SearchTriggerComponent', () => {
  let component: SearchTriggerComponent;
  let fixture: ComponentFixture<SearchTriggerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
          SearchTriggerComponent
      ],
      imports: [
          FormsModule,
          ReactiveFormsModule
      ]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchTriggerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
