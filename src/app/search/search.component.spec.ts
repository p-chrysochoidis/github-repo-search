/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { SearchComponent } from "./search.component";
import { SearchTriggerComponent } from "./search-trigger/search-trigger.component";
import { SearchResultsListComponent } from "./search-results-list/search-results-list.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SearchResultItemComponent } from "./search-result-item/search-result-item.component";
import { AppRoutingModule } from "../app-routing.module";
import { RepositoryModule } from "../repository/repository.module";
import { CommonModule } from "../shared/common.module";
import { APP_BASE_HREF } from "@angular/common";
import { Http, BaseRequestOptions, ConnectionBackend } from "@angular/http";
import { MockBackend } from "@angular/http/testing";

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;

  beforeEach(async(() => {
    //TODO: should configure Testing module better
    TestBed.configureTestingModule({
      declarations: [
        SearchComponent,
        SearchTriggerComponent,
        SearchResultsListComponent,
        SearchResultItemComponent,
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        AppRoutingModule,
        RepositoryModule,
        CommonModule
      ],
      providers: [
        {provide: APP_BASE_HREF, useValue: "/"},
        BaseRequestOptions,
        MockBackend,
        {
          provide: Http,
          useFactory: (backEnd: ConnectionBackend, defaultOptions: BaseRequestOptions) => {
            return new Http(backEnd, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        }
      ]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
