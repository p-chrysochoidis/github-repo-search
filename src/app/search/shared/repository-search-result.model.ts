/**
 * Created by pavlos on 20/12/2016.
 *
 * Represents the result of each repository of the {@link GithubApiService.searchRepository} call.
 */
export class RepositorySearchResult {
  //TODO: properties should be private
  public id: number;
  public name: string;
  public owner: string;
  public fullName: string;
  public htmlUrl: string;
  public description: string;
  public stargazersCount: number;
  public forksCount: number;
  public openIssuesCount: number;
  public language: string;
  public score: number;


  constructor(resultJson: any) {
    this.id = resultJson.id;
    this.name = resultJson.name;
    this.owner = resultJson.owner.login;
    this.fullName = resultJson.full_name;
    this.htmlUrl = resultJson.html_url;
    this.description = resultJson.description;
    this.stargazersCount = resultJson.stargazers_count;
    this.forksCount = resultJson.forks_count;
    this.openIssuesCount = resultJson.open_issues_count;
    this.language = resultJson.language;
    this.score = resultJson.score;
  }

}