/**
 * Created by pavlos on 18/12/2016.
 */
import { SearchComponent } from "./search.component";
import { NgModule } from "@angular/core";
import { SearchTriggerComponent } from './search-trigger/search-trigger.component';
import { SearchResultsListComponent } from './search-results-list/search-results-list.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { SearchResultItemComponent } from './search-result-item/search-result-item.component';
import { RouterModule } from "@angular/router";
import { CommonModule } from "../shared/common.module";

@NgModule({
  declarations: [
    SearchComponent,
    SearchTriggerComponent,
    SearchResultsListComponent,
    SearchResultItemComponent,
  ],
  exports: [
    SearchComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CommonModule
  ]
})
export class SearchModule {

}
