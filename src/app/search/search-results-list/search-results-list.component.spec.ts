/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { SearchResultsListComponent } from "./search-results-list.component";
import { SearchResultItemComponent } from "../search-result-item/search-result-item.component";
import { RouterModule } from "@angular/router";
import { CommonModule } from "../../shared/common.module";

describe('SearchResultsListComponent', () => {
  let component: SearchResultsListComponent;
  let fixture: ComponentFixture<SearchResultsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
          SearchResultsListComponent,
          SearchResultItemComponent
      ],
      imports: [
          RouterModule,
          CommonModule
      ]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchResultsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
