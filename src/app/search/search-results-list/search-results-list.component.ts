import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { RepositorySearchResult } from "../shared/repository-search-result.model";
/**
 * A component that renders the repositories of the search result {@link RepositorySearchResult}
 *
 * Inputs:
 * searchResults {RepositorySearchResult[]} The array with the {@link RepositorySearchResult} to display.
 */
@Component({
  selector: 'app-search-results-list',
  templateUrl: './search-results-list.component.html',
  styleUrls: ['./search-results-list.component.scss']
})
export class SearchResultsListComponent implements OnInit, OnChanges {
  //XXX: Change and grid number of the template in case of changing this
  private static ITEMS_PER_ROW: number = 3;
  @Input()
  private searchResults: RepositorySearchResult[];
  private searchResultLines: RepositorySearchResult[][];

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges){
    this.arrangeSearchResultsInRows();
  }

  ngOnInit() {
  }

  /**
   * Arranges the {@link searchResults} in {@link ITEMS_PER_ROW} items per row
   * and saves the result in the {@link searchResultLines} variable
   */
  private arrangeSearchResultsInRows(){
    let resPerLine: RepositorySearchResult[][] = [],
        currentLine: RepositorySearchResult[],
        lineItemsCount: number = 0;

    this.searchResults.forEach((aResult) => {
      if(lineItemsCount % SearchResultsListComponent.ITEMS_PER_ROW == 0){
        currentLine = [];
        resPerLine.push(currentLine);
        lineItemsCount = 0;
      }
      currentLine.push(aResult);
      lineItemsCount++;
    });
    this.searchResultLines = resPerLine;
  }
}