import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subject, Subscription, Observable } from "rxjs";
import { Router, ActivatedRoute } from "@angular/router";
import { GithubApiService } from "../shared/github-api.service";
import { RepositorySearchResult } from "./shared/repository-search-result.model";
import { ObservableUtils } from "../shared/observable-utils";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  private forceRouteQueryChange: Subject<string> = new Subject<string>();
  private forceSearch: Subject<string> = new Subject<string>();
  private hasMore:boolean;
  private loading: boolean;
  private nextPageSubscription: Subscription;
  private page: number = 1;
  private queryParamName: string = "q";
  private routeChangeError: boolean = false;
  private searchError: boolean = false;
  private searchPrompt: string = "Search a repository";
  private searchQueryStream: Subject<string> = new Subject<string>();
  private searchQuerySubscription: Subscription;
  private searchResults: RepositorySearchResult[] = [];
  private searchSubscription: Subscription;
  private searchTerm: string;

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private githubApiService: GithubApiService
  ) {
    this.initQuery(activatedRoute);
  }

  ngOnInit() {
    this.subscribeForQueryChanges();
    this.subscribeForSearch();
  }

  ngOnDestroy() {
    this.unsubscribeFromQueryChanges();
    this.unsubscribeFromSearch();
  }

  /**
   * Initialize the {@link searchTerm} from the current route query params.
   */
  private initQuery(activatedRoute: ActivatedRoute) {
    this.searchTerm = activatedRoute.snapshot.queryParams[this.queryParamName] || "";
    this.searchTerm = this.searchTerm.trim();
  }

  /**
   * Loads more repositories for the current search term
   */
  private loadNextPage(){
    this.loading = true;
    this.page++;
    this.nextPageSubscription = this.githubApiService.searchRepository(this.searchTerm, this.page)
        .subscribe(
            searchResults => this.renderResults(searchResults, true),
            error => {
              console.log(`There was na error loading next page`, error);
              this.page--;
            }
        );
  }

  /**
   * Called when user writes a new search query.
   * Calls next on the {@link searchQueryStream} with the new value after
   * trimming the query string.
   * @param newQuery string
   */
  private onSearchQueryChanged(newQuery: string) {
    newQuery = newQuery || "";
    newQuery = newQuery.trim();
    this.searchQueryStream.next(newQuery);
  }

  /**
   * Called from the html template and depending the error {@link routeChangeError}
   * {@link searchError} it recovers.
   */
  private recoverFromError(){
    if(this.routeChangeError){
      this.routeChangeError = false;
      this.forceRouteQueryChange.next(this.searchTerm);
    }else if(this.searchError){
      this.searchError = false;
      this.forceSearch.next(this.searchTerm);
    }
  }

  /**
   * Render the search results
   * @param searchResult The results
   * @param append Indicates if the results should be appended in the current results
   */
  private renderResults(searchResult: RepositorySearchResult[], append: boolean = false) {
    this.hasMore = searchResult.length > 0;
    if(append){
      searchResult = this.searchResults.concat(searchResult);
    }
    this.searchResults = searchResult;
    this.loading = false;
  }

  /**
   * Updates the queryParams of the route and performs a search request.
   * @param query string The search query
   * @returns Observable<string> An observable emitting the search query.
   */
  private setSearchRouteQueryParams(query: string): Observable<string> {
    let queryParams = {};
    if (query && query.length) {
      queryParams[this.queryParamName] = query;
    }
    this.routeChangeError = false;
    return ObservableUtils.retryWithIncrementalBackOff(Observable
        .fromPromise(this.router.navigate(['/search'], {queryParams: queryParams}))
        .map(_ => query)
    );
  }

  /**
   * Use the {@link GithubApiService} to search for repositories
   * @param query string Search term
   * @returns Observable<RepositorySearchResult[]>
   */
  private search(query: string): Observable<RepositorySearchResult[]> {
    if (query && query.length) {
      return this.githubApiService.searchRepository(query);
    }
    return Observable.of([]);
  }

  /**
   * Called when search query is changed in order to initialize some common
   * variables and stop next page call (if any)
   */
  private startedLoading(){
    this.unsubscribeFromNextPage();
    this.loading = true;
    this.searchResults = [];
    this.page = 1;
    this.hasMore = false;
    this.routeChangeError = false;
    this.searchError = false;
  }

  /**
   * Subscribes on {@link searchQueryStream} and triggers a search action by
   * changing the route.
   */
  private subscribeForQueryChanges() {
    this.searchQuerySubscription = this.searchQueryStream
        .distinctUntilChanged()
        .merge(this.forceRouteQueryChange)
        .do(query => {
          this.searchTerm = query;
        })
        .map((query) => this.setSearchRouteQueryParams(query))
        .switch()
        .subscribe(
            () => { },
            (e) => {
              console.log(`Error: ${e}`);
              this.unsubscribeFromQueryChanges();
              this.subscribeForQueryChanges();
              this.routeChangeError = true;
              this.hasMore = false;
              this.searchResults = [];
              this.loading = false;
            },
            () => console.log("Completed")
        );
  }

  /**
   * Subscribes for changes to the {@link ActivatedRoute} changes of the
   * query params and triggers a search for a repository.
   * @param skipFirst <boolean> Indicates if the first value should be skipped
   */
  private subscribeForSearch(skipFirst: boolean = false) {
    this.searchSubscription = this.activatedRoute.queryParams
        .skip(skipFirst ? 1 : 0)
        .map((queryParams) => {
          let searchTerm = queryParams[this.queryParamName];
          this.searchTerm = searchTerm;
          return searchTerm;
        })
        .merge(this.forceSearch)
        .do(() => this.startedLoading())
        .map(searchTerm => this.search(searchTerm))
        .switch()
        .subscribe(
            (searchResults) => this.renderResults(searchResults),
            (e) => {
              console.log(`Error: ${e}`);
              this.loading = false;
              this.unsubscribeFromSearch();
              this.subscribeForSearch(true);
              this.searchError = true;
            },
            () => console.log("Completed")
        );
  }

  /**
   * Unsubscribes from {@link nextPageSubscription}, if there is any subscription.
   */
  private unsubscribeFromNextPage() {
    if (this.nextPageSubscription) {
      this.nextPageSubscription.unsubscribe();
    }
    this.nextPageSubscription = null;
  }

  /**
   * Unsubscribes from query changes, if there is any subscription.
   */
  private unsubscribeFromQueryChanges() {
    if (this.searchQuerySubscription) {
      this.searchQuerySubscription.unsubscribe();
    }
    this.searchQuerySubscription = null;
  }

  /**
   * Unsubscribes from {@link ActivatedRoute} changes {@see searchSubscription}, if there is any subscription.
   */
  private unsubscribeFromSearch() {
    if (this.searchSubscription) {
      this.searchSubscription.unsubscribe();
    }
    this.searchSubscription = null;
  }
}