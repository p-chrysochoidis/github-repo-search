/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { SearchResultItemComponent } from "./search-result-item.component";
import { CommonModule } from "../../shared/common.module";
import { RepositorySearchResult } from "../shared/repository-search-result.model";

describe('SearchResultItemComponent', () => {
  let component: SearchResultItemComponent;
  let fixture: ComponentFixture<SearchResultItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SearchResultItemComponent
      ],
      imports: [
        CommonModule
      ]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchResultItemComponent);
    component = fixture.componentInstance;
    component.searchResult = new RepositorySearchResult({
      id: 1,
      name: "Test Repo",
      owner: {login: "Owner"},
      full_name: "Full Name",
      html_url: "htmlUrl",
      description: "Description",
      stargazers_count: 10,
      forks_count: 100,
      open_issues_count: 3,
      language: "Java",
      score: 30
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
