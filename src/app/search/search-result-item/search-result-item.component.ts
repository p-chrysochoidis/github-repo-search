import { Component, OnInit, Input } from '@angular/core';
import { RepositorySearchResult } from "../shared/repository-search-result.model";

/**
 * A component representing a repository search result
 *
 * Inputs:
 * searchResult: RepositorySearchResult The search result data to be rendered
 */
@Component({
  selector: 'app-search-result-item',
  templateUrl: './search-result-item.component.html',
  styleUrls: ['./search-result-item.component.scss']
})
export class SearchResultItemComponent implements OnInit {
  @Input()
  public searchResult: RepositorySearchResult;

  constructor() { }

  ngOnInit() {
  }

}
