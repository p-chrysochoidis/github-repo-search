import { Component, OnInit, Input, OnChanges, SimpleChanges, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { RepositoryDetails } from "../shared/repository-details.model";
import { GithubApiService } from "../../shared/github-api.service";
import { RepositoryCommits } from "../shared/repositiry-commits.model";
import { Subscription } from "rxjs";
/**
 * A component for showing to the user the details of a
 * repository.
 *
 * Inputs:
 * repository: The {@link RepositoryDetails} object
 */
@Component({
  selector: 'app-repository-details',
  templateUrl: './repository-details.component.html',
  styleUrls: ['./repository-details.component.scss']
})
export class RepositoryDetailsComponent implements OnInit, OnChanges, OnDestroy {
  private commitsChart;
  private loadingCommitsChart: boolean = true;
  @Input()
  private repository: RepositoryDetails;
  private pieChart;
  private commitsSubscription: Subscription;

  constructor(
      private githubApi: GithubApiService
  ) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.initializePieChart();
    this.initializeCommitsChart();
  }

  ngOnDestroy(){
    this.unsubscribeFromCommits();
  }

  ngOnInit() {
  }

  private initializePieChart() {
    if (!this.repository) {
      return;
    }
    this.pieChart = {
      data: [
        this.repository.forksCount,
        this.repository.watchersCount,
        this.repository.subscribersCount,
        this.repository.stargazersCount,
        this.repository.openIssuesCount
      ],
      labels: [
        "Forks",
        "Watchers",
        "Subscribers",
        "Stargazers",
        "Open Issues"
      ],
      type: 'pie'
    };
  }

  private initializeCommitsChart() {
    if (!this.repository) {
      return;
    }
    this.unsubscribeFromCommits();
    this.commitsSubscription = this.githubApi.repositoryCommitsOfLast52Weeks(this.repository.owner.name, this.repository.name)
        .subscribe(
            commits => this.renderCommitsChart(commits),
            error => console.log("error showing commits")
        );//TODO: handle errors
  }

  /**
   * Initializes the {@link commitsChart} using the given {@link RepositoryCommits}
   *
   * @param commits {RepositoryCommits}
   */
  private renderCommitsChart(commits: RepositoryCommits) {
    this.commitsChart = {
      data: [
        {data: commits.ownersCommits, label: "Commits of owner"},
        {data: commits.othersCommits, label: "Commits of others"}
      ],
      labels: commits.ownersCommits.map((val, index) => index+1),
      type: "line"
    };
    this.loadingCommitsChart = false;
  }

  /**
   * Unsubscribe from the {@link commitsSubscription} if there is any
   */
  private unsubscribeFromCommits(){
    if(this.commitsSubscription){
      this.commitsSubscription.unsubscribe();
      this.commitsSubscription = null;
    }
  }
}
