/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { RepositoryDetailsComponent } from "./repository-details.component";
import { RepositoryOwnerComponent } from "../repository-owner/repository-owner.component";
import { CommonModule } from "../../shared/common.module";
import { ChartsModule } from "ng2-charts";
import { RepositoryIssuesComponent } from "../repository-issues/repository-issues.component";
import { RepositoryIssueComponent } from "../repository-issue/repository-issue.component";
import { BaseRequestOptions, Http, ConnectionBackend } from "@angular/http";
import { MockBackend } from "@angular/http/testing";

describe('RepositoryDetailsComponent', () => {
  let component: RepositoryDetailsComponent;
  let fixture: ComponentFixture<RepositoryDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RepositoryDetailsComponent,
        RepositoryOwnerComponent,
        RepositoryIssuesComponent,
        RepositoryIssueComponent
      ],
      imports: [
        CommonModule,
        ChartsModule
      ],
      providers: [
        BaseRequestOptions,
        MockBackend,
        {
          provide: Http,
          useFactory: (backEnd: ConnectionBackend, defaultOptions: BaseRequestOptions) => {
            return new Http(backEnd, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        }
      ]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepositoryDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
