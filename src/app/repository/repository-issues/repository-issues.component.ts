import { Component, OnInit, Input } from '@angular/core';
import { RepositoryDetails } from "../shared/repository-details.model";
import { Observable } from "rxjs";
import { GithubApiService } from "../../shared/github-api.service";
import { RepositoryIssue } from "../shared/repository-issue";
/**
 * A component responsible for shown the issues of a given repository.
 *
 * Input:
 * repository: The {@link RepositoryDetails} to show it's issues
 */
@Component({
  selector: 'app-repository-issues',
  templateUrl: './repository-issues.component.html',
  styleUrls: ['./repository-issues.component.scss']
})
export class RepositoryIssuesComponent implements OnInit {
  private issues: Observable<RepositoryIssue[]>;
  private loading: boolean;
  @Input()
  public repository: RepositoryDetails;

  constructor(private githubApi: GithubApiService) {
    this.loading = true;
  }

  ngOnInit() {
    this.loadIssues();
  }

  private loadIssues(append: boolean = false){
    this.loading = true;
    this.issues = this.githubApi.repositoryIssues(this.repository.owner.name, this.repository.name)//TODO: add page
        .do(issues => {
          this.loading = false;
        });
  }

}
