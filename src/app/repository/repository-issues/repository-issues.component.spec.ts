/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { RepositoryIssuesComponent } from "./repository-issues.component";
import { CommonModule } from "../../shared/common.module";
import { RepositoryIssueComponent } from "../repository-issue/repository-issue.component";
import { BaseRequestOptions, Http, ConnectionBackend } from "@angular/http";
import { MockBackend } from "@angular/http/testing";
import { RepositoryDetails } from "../shared/repository-details.model";
import { RepositoryOwner } from "../shared/repository-owner.model";

describe('RepositoryIssuesComponent', () => {
  let component: RepositoryIssuesComponent;
  let fixture: ComponentFixture<RepositoryIssuesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RepositoryIssuesComponent,
        RepositoryIssueComponent
      ],
      imports: [CommonModule],
      providers: [
        BaseRequestOptions,
        MockBackend,
        {
          provide: Http,
          useFactory: (backEnd: ConnectionBackend, defaultOptions: BaseRequestOptions) => {
            return new Http(backEnd, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        }
      ]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepositoryIssuesComponent);
    component = fixture.componentInstance;
    component.repository = new RepositoryDetails({
      id: 1,
      description: "Description",
      forks_count: 1,
      full_name: "Full Name",
      html_url: "htmlUrl",
      fork: false,
      private: false,
      language: "Java",
      name: "name",
      open_issues_count: 32,
      owner: {
        id: 1,
        name: "owner",
        avatar_url: "avater",
        html_url: "url"
      },
      stargazers_count: 50,
      subscribers_count: 30,
      watchers_count: 10
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
