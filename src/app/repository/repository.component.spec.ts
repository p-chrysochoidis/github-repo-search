/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { ActivatedRoute } from "@angular/router";
import { RepositoryComponent } from "./repository.component";
import { CommonModule } from "../shared/common.module";
import { RepositoryDetailsComponent } from "./repository-details/repository-details.component";
import { RepositoryOwnerComponent } from "./repository-owner/repository-owner.component";
import { ChartsModule } from "ng2-charts";
import { RepositoryIssueComponent } from "./repository-issue/repository-issue.component";
import { RepositoryIssuesComponent } from "./repository-issues/repository-issues.component";
import { BaseRequestOptions, Http, ConnectionBackend } from "@angular/http";
import { MockBackend } from "@angular/http/testing";

describe('RepositoryComponent', () => {
  let component: RepositoryComponent;
  let fixture: ComponentFixture<RepositoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RepositoryComponent,
        RepositoryDetailsComponent,
        RepositoryOwnerComponent,
        RepositoryIssuesComponent,
        RepositoryIssueComponent
      ],
      imports: [
        CommonModule,
        ChartsModule
      ],
      providers: [
        {
          provide: ActivatedRoute, useValue: {
            snapshot: {
              params: {
                owner: "owner",
                name: "name"
              }
            }
          }
        },
        BaseRequestOptions,
        MockBackend,
        {
          provide: Http,
          useFactory: (backEnd: ConnectionBackend, defaultOptions: BaseRequestOptions) => {
            return new Http(backEnd, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        }
      ]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepositoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
