/**
 * Created by pavlos on 23/12/2016.
 *
 * Represents an issue of a repository. The result of github api call GET /repos/:owner/:repo/issues
 */
export class RepositoryIssue{
  //TODO: properties should be private
  public id: number;
  public body: string;
  public commentsCount: number;
  public htmlUrl: string;
  public number: string;
  public state: string;
  public title: string;

  constructor(jsonResponse:any){
    this.id = jsonResponse.id;
    this.body = jsonResponse.body;
    this.commentsCount = jsonResponse.comments;
    this.htmlUrl = jsonResponse.html_url;
    this.number = jsonResponse.number;
    this.state = jsonResponse.state;
    this.title = jsonResponse.title;
  }
}