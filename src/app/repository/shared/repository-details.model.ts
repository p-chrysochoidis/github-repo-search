import { RepositoryOwner } from "./repository-owner.model";
/**
 * Created by pavlos on 22/12/2016.
 *
 * A model representing a github repository.
 * This is the result of the github api call: /repos/:owner/:repo
 */
export class RepositoryDetails{
  //TODO: properties should be private
  public id: number;
  public description: string;
  public forksCount: number;
  public fullName: string;
  public htmlUrl: string;
  public isFork: boolean;
  public isPrivate: boolean;
  public language: string;
  public name: string;
  public openIssuesCount: number;
  public owner: RepositoryOwner;
  public stargazersCount: number;
  public subscribersCount: number;
  public watchersCount: number;

  /**
   * Initialize the object using the json response of the github api call /repos/:owner/:repo
   * @param jsonResponse
   */
  constructor(jsonResponse: any){
    this.id = jsonResponse.id;
    this.description = jsonResponse.description;
    this.forksCount = jsonResponse.forks_count;
    this.fullName = jsonResponse.full_name;
    this.htmlUrl = jsonResponse.html_url;
    this.isFork = jsonResponse.fork;
    this.isPrivate = jsonResponse.private;
    this.language = jsonResponse.language;
    this.name = jsonResponse.name;
    this.openIssuesCount = jsonResponse.open_issues_count;
    this.owner = new RepositoryOwner(jsonResponse.owner);
    this.stargazersCount = jsonResponse.stargazers_count;
    this.subscribersCount = jsonResponse.subscribers_count;
    this.watchersCount = jsonResponse.watchers_count;
  }
}