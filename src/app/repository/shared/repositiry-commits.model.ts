/**
 * Created by pavlos on 23/12/2016.
 *
 * Represents the result of the github api call GET /repos/:owner/:repo/stats/participation.
 */
export class RepositoryCommits{
  private all: number[];
  private owner: number[];

  /**
   * Initializes the object from the json result of the call {@link GithubApiService#repositoryCommitsOfLast52Weeks}
   * @param jsonResponse
   */
  constructor(jsonResponse: any){
    this.all = jsonResponse.all;
    this.owner = jsonResponse.owner;
  }


  public get othersCommits(){
    return this.all;
  }

  public get ownersCommits(){
    return this.owner;
  }
}