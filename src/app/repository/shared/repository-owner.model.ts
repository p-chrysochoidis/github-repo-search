/**
 * Created by pavlos on 22/12/2016.
 * Represents an owner of a github repository.
 * See the <b>owner</b> of the github api call /repos/:owner/:repo
 */
export class RepositoryOwner{
  //TODO: properties should be private
  public id: number;
  /**
   * The <b>login</b> field of the result.
   */
  public name: string;
  public avatarUrl: string;
  public htmlUrl: string;

  /**
   * Initialize the object with the content of the json field <b>owner</b> of the /repos/:owner/:repo api call
   * @param jsonResult json
   */
  constructor(jsonResult: any){
    this.id = jsonResult.id;
    this.name = jsonResult.login;
    this.avatarUrl = jsonResult.avatar_url;
    this.htmlUrl = jsonResult.html_url;
  }
}