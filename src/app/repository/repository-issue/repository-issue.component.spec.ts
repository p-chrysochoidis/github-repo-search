/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { RepositoryIssueComponent } from "./repository-issue.component";
import { CommonModule } from "../../shared/common.module";

describe('RepositoryIssueComponent', () => {
  let component: RepositoryIssueComponent;
  let fixture: ComponentFixture<RepositoryIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RepositoryIssueComponent],
      imports: [CommonModule]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepositoryIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
