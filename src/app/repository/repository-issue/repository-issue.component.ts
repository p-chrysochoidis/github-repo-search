import { Component, OnInit, Input } from '@angular/core';
import { RepositoryIssue } from "../shared/repository-issue";
/**
 * Renders a {@link RepositoryIssue}
 *
 * Input:
 * issue The {@link RepositoryIssue} to render
 */
@Component({
  selector: 'app-repository-issue',
  templateUrl: './repository-issue.component.html',
  styleUrls: ['./repository-issue.component.scss']
})
export class RepositoryIssueComponent implements OnInit {
  @Input()
  private issue: RepositoryIssue;

  constructor() { }

  ngOnInit() {
  }

}
