import { NgModule } from '@angular/core';
import { BrowserModule } from "@angular/platform-browser";
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { RepositoryComponent } from "./repository.component";
import { CommonModule } from "../shared/common.module";
import { RepositoryDetailsComponent } from "./repository-details/repository-details.component";
import { RepositoryOwnerComponent } from './repository-owner/repository-owner.component';
import { RepositoryIssuesComponent } from './repository-issues/repository-issues.component';
import { RepositoryIssueComponent } from './repository-issue/repository-issue.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    ChartsModule
  ],
  declarations: [
    RepositoryComponent,
    RepositoryDetailsComponent,
    RepositoryOwnerComponent,
    RepositoryIssuesComponent,
    RepositoryIssueComponent
  ],
  exports: [
    RepositoryComponent
  ],
})
export class RepositoryModule {
}
