import { Component, OnInit, Input } from '@angular/core';
import { RepositoryOwner } from "../shared/repository-owner.model";
/**
 * A component for displaying a repository's owner
 *
 * Inputs:
 * owner: A {@link RepositoryOwner}
 */
@Component({
  selector: 'app-repository-owner',
  templateUrl: './repository-owner.component.html',
  styleUrls: ['./repository-owner.component.scss']
})
export class RepositoryOwnerComponent implements OnInit {
  @Input()
  private owner: RepositoryOwner;
  constructor() { }

  ngOnInit() {
  }

}
