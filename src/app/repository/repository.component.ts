import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { GithubApiService } from "../shared/github-api.service";
import { Observable } from "rxjs";
import { RepositoryDetails } from "./shared/repository-details.model";
/**
 * Used to render a repository's details.
 */
@Component({
  selector: 'app-repository',
  templateUrl: './repository.component.html',
  styleUrls: ['./repository.component.scss']
})
export class RepositoryComponent implements OnInit {
  private loading: boolean = true;
  private repositoryParams:{
    owner: string,
    name: string
  };
  private repository: Observable<RepositoryDetails>;

  constructor(
      activatedRoute: ActivatedRoute,
      private githubService: GithubApiService
  ) {
    this.initRepositoryParams(activatedRoute);
  }

  ngOnInit() {
    this.loadRepositoryDetails();
  }

  /**
   * Initializes the {@link repositoryParams} from the {@link ActivatedRoute}
   * @param activatedRoute
   */
  private initRepositoryParams(activatedRoute: ActivatedRoute){
    let params = activatedRoute.snapshot.params;
    this.repositoryParams = {
      owner: params['owner'],
      name: params['name']
    };
  }

  /**
   * Uses the {@link GithubApiService} to get the details of the given repository {@link repositoryParams}
   */
  private loadRepositoryDetails(){
    this.repository = this.githubService
        .repositoryDetails(this.repositoryParams.owner, this.repositoryParams.name)
        .do(repositoryDetails => {
          this.loading = false;
        });
  }
}
