/* tslint:disable:no-unused-variable */
import { TestBed, inject, fakeAsync, tick } from "@angular/core/testing";
import { GithubApiService } from "./github-api.service";
import { BaseRequestOptions, Http, ConnectionBackend, ResponseOptions, Response } from "@angular/http";
import { MockBackend } from "@angular/http/testing";

describe('GithubApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        GithubApiService,
        BaseRequestOptions,
        MockBackend,
        {
          provide: Http,
          useFactory: (backEnd: ConnectionBackend, defaultOptions: BaseRequestOptions) => {
            return new Http(backEnd, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        }
      ]
    });
  });

  it('should be injected', inject([GithubApiService], (service: GithubApiService) => {
    expect(service).toBeTruthy();
  }));

  it('should search for repositories', inject([GithubApiService, MockBackend], fakeAsync((githubApi, backEnd) => {
        let searchTerm = "saferandom",
            page = 1;
        backEnd.connections.subscribe(connection => {
          let expectedUrl = `https://api.github.com/search/repositories?q=${searchTerm}&page=${page}`;
          expect(connection.request.url).toBe(expectedUrl);
          let response = new ResponseOptions({
            body: JSON.stringify({
              items: [
                {
                  id: 1,
                  name: "Test repository",
                  owner: {login: "Owner"},
                  full_name: "Full Name",
                  html_url: "httpUrl",
                  description: "Description",
                  stargazers_count: 10,
                  forks_count: 1,
                  open_issues_count: 1,
                  language: "Javascript",
                  score: 10
                }
              ]
            })
          });
          connection.mockRespond(new Response(response));
        });
        let result;
        githubApi.searchRepository(searchTerm, page).subscribe((res) => {
          result = res;
        });
        tick();
        expect(result).toBeTruthy();
        expect(result.length).toBe(1);
        expect(result[0].name).toBe("Test repository");
      }))
  );
});
