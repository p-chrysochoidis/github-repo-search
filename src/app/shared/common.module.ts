import { NgModule } from '@angular/core';
import { GithubApiService } from "./github-api.service";
import { LoaderComponent } from './loader/loader.component';
import { StatsIndicatorComponent } from './stats-indicator/stats-indicator.component';
import { AlertComponent } from './alert/alert.component';
import { BrowserModule } from "@angular/platform-browser";

@NgModule({
  imports: [
    BrowserModule
  ],
  providers: [
    GithubApiService
  ],
  declarations: [
    LoaderComponent,
    StatsIndicatorComponent,
    AlertComponent
  ],
  exports: [
    LoaderComponent,
    StatsIndicatorComponent,
    AlertComponent
  ]
})
export class CommonModule {
}
