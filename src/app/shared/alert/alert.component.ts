import { Component, OnInit, Input } from '@angular/core';
/**
 * Implements the alert component of bootstrap
 *
 * Inputs:
 * {@link alertType} <string> One of
 *    'alert-dismissible'
 *    'alert-success'
 *    'alert-info'
 *    'alert-warning'
 *    'alert-danger'
 * {@link dismissible} <boolean> Indicates if the user can close the alert
 */
@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  @Input()
  private alertType: string = 'alert-info';
  private classes: {
    'alert-dismissible': boolean,
    'alert-success'?: boolean,
    'alert-info'?: boolean,
    'alert-warning'?: boolean,
    'alert-danger'?: boolean,
  };
  @Input()
  private dismissible: boolean = false;

  constructor() {

  }

  ngOnInit() {
    this.classes = {
      'alert-dismissible': this.dismissible
    };
    this.classes[this.alertType] = true;
  }

}
