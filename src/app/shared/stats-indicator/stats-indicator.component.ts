import { Component, OnInit, Input } from '@angular/core';
/**
 * A component used to display a number and a text as a title below
 *
 * Inputs:
 * numberIndicator: The number to show
 * textIndicator: The text to show
 */
@Component({
  selector: 'app-stats-indicator',
  templateUrl: './stats-indicator.component.html',
  styleUrls: ['./stats-indicator.component.scss']
})
export class StatsIndicatorComponent implements OnInit {
  @Input()
  private numberIndicator: number;
  @Input()
  private  textIndicator: string;

  constructor() { }

  ngOnInit() {
  }

}
