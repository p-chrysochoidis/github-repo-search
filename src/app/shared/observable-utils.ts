import { Observable } from "rxjs";
/**
 * Created by pavlos on 24/12/2016.
 * Utility functions for observables
 */
export class ObservableUtils{
  /**
   * Used to determine the time between each retry.
   * @type {number}
   */
  private static DELAY_BETWEEN_RETRIES_MILLIS = 200;

  /**
   * Retries the {@see stream} when there is an error for {@see maxTries} - 1, so there would be for maximum
   * {@see maxTries} tries.
   *
   * @param stream The {@link Observable} to retry when there is an error
   * @param maxTries The total number of tries
   * @return {Observable<T>}
   */
  public static retryWithIncrementalBackOff<T>(stream: Observable<T>, maxTries: number = 5):Observable<T>{
    return stream.retryWhen(errors => {
      return errors.zip(Observable.range(1, maxTries))
          .map(val => {
            if(val[1] >= maxTries){
              throw val[0];
            }
            return val;
          })
          .delayWhen(val => Observable.timer(val[1] * ObservableUtils.DELAY_BETWEEN_RETRIES_MILLIS));
    });
  }
}