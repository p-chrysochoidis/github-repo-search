import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";
import { Observable } from "rxjs";
import { RepositorySearchResult } from "../search/shared/repository-search-result.model";
import { RepositoryDetails } from "../repository/shared/repository-details.model";
import { RepositoryIssue } from "../repository/shared/repository-issue";
import { RepositoryCommits } from "../repository/shared/repositiry-commits.model";
import { ObservableUtils } from "./observable-utils";
/**
 * A service for accessing the api of github.
 * Every call automatically retries with incremental back-off in case of error.
 */
@Injectable()
export class GithubApiService {
  private static GITHUB_API_URL: string = "https://api.github.com/";

  constructor(private http: Http) {
  }

  /**
   * Performs a call to the github api and returns all the repositories for
   * the specific search term.
   *
   * @param query
   * @param page
   * @returns {Observable<RepositorySearchResult[]>}
   */
  public searchRepository(query: string, page: number = 1): Observable<RepositorySearchResult[]> {
    let searchUrl = `${GithubApiService.GITHUB_API_URL}search/repositories?q=${query}&page=${page}`,
        observable = this.http.get(searchUrl)
            .map(result => {
              return result.json();
            })
            .map((resultsJson: any) => {
              return resultsJson.items.map((item) => {
                return new RepositorySearchResult(item);
              });
            });
    return ObservableUtils.retryWithIncrementalBackOff(observable);
  }

  /**
   * Returns the number of commits per week of the owner and all the others.
   * The result contains data for the last 52 weeks
   *
   * @param owner {string} The owner of the repository
   * @param name {number} The page to fetch
   * @returns {Observable<RepositoryCommits>}
   */
  public repositoryCommitsOfLast52Weeks(owner: string, name: string): Observable<RepositoryCommits> {
    let url = `${GithubApiService.GITHUB_API_URL}repos/${owner}/${name}/stats/participation`,
        observable = this.http.get(url)
            .map(res => res.json())
            .map(resJson => {
              if(!resJson.all || !resJson.owner){
                throw `Empty response ${JSON.stringify(resJson)}`;
              }
              return new RepositoryCommits(resJson);
            });
    return ObservableUtils.retryWithIncrementalBackOff(observable);
  }

  /**
   * Get the details for the given repository
   *
   * @param owner The owner of the repository
   * @param name The name of the repository
   * @returns {Observable<RepositoryDetails>}
   */
  public repositoryDetails(owner: string, name: string): Observable<RepositoryDetails> {
    let url = `${GithubApiService.GITHUB_API_URL}repos/${owner}/${name}`,
        observable = this.http.get(url)
            .map(response => response.json())
            .map(responseJson => new RepositoryDetails(responseJson));
    return ObservableUtils.retryWithIncrementalBackOff(observable);
  }

  /**
   * Get the issues of a repository
   *
   * @param owner {string} The owner of the repository
   * @param name {string} The name of the repository
   * @param page {number} The page to fetch
   * @returns {Observable<RepositoryIssue[]>}
   */
  public repositoryIssues(owner: string, name: string, page: number = 1): Observable<RepositoryIssue[]> {
    let url = `${GithubApiService.GITHUB_API_URL}repos/${owner}/${name}/issues?page=${page}`,
        observable = this.http.get(url)
        .map(response => response.json())
        .map(resJson => resJson.map(anIssue => new RepositoryIssue(anIssue)));
    return ObservableUtils.retryWithIncrementalBackOff(observable);
  }
}