import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { AppComponent } from "./app.component";
import { AppLogoComponent } from "./app-logo/app-logo.component";
import { AppRoutingModule } from "./app-routing.module";
import { SearchModule } from "./search/search.module";
import { CommonModule } from "./shared/common.module";
import { RepositoryModule } from "./repository/repository.module";

@NgModule({
  declarations: [
    AppComponent,
    AppLogoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    SearchModule,
    CommonModule,
    RepositoryModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
