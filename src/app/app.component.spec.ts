/* tslint:disable:no-unused-variable */
import { TestBed, async } from "@angular/core/testing";
import { AppComponent } from "./app.component";
import { AppLogoComponent } from "./app-logo/app-logo.component";
import { AppRoutingModule } from "./app-routing.module";
import { SearchModule } from "./search/search.module";
import { RepositoryModule } from "./repository/repository.module";
import { APP_BASE_HREF } from "@angular/common";

describe('AppComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        AppLogoComponent
      ],
      imports: [
        AppRoutingModule,
        SearchModule,
        RepositoryModule
      ],
      providers: [
        {provide: APP_BASE_HREF, useValue: "/"},
      ]
    });
    TestBed.compileComponents();
  });

  it('should create the app', async(() => {
    let fixture = TestBed.createComponent(AppComponent);
    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

});
