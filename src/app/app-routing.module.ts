/**
 * Created by pavlos on 18/12/2016.
 */
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SearchComponent } from "./search/search.component";
import { RepositoryComponent } from "./repository/repository.component";


let routes: Routes = [
  {path: "search", component: SearchComponent},
  {path: "repository/:owner/:name", component: RepositoryComponent},
  {path: "", redirectTo: "/search", pathMatch: "full"}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}